import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.sql.Array;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by ����� on 11.10.2015.
 */
public class SimpleRingBuffer<T> implements IRingBuffer<T> {

    private T[] buff; // buffer of elements

    private final int size; // buffer length
    private int head;
    private int tail;
    private int currentSize; // current size

    public SimpleRingBuffer(Integer count) {
        buff = (T[]) new Object[count];

        head = 0;
        tail = 0;
        size = count;
    }

    public synchronized void add(T value)  {
        while (this.size() == buff.length)
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        if(head != (tail - 1) && head < size)
            buff[head++] = value;
        else
            throw new BufferOverflowException();

        currentSize++;
        head = head % buff.length;
        notifyAll();
    }

    public T get() {
        T t = null;
        int adjTail = tail > head ? tail - buff.length : tail;
        if(adjTail < head) {
            t = (T) buff[tail++];
            tail = tail % buff.length;
        } else {
            throw new BufferUnderflowException();
        }
        currentSize--;
        return t;
    }

    public  boolean remove(T value) {
        for (int i = 0; i < size; i++) {
            for(;i < size; ++i)
                if (buff[i] == value) {
                    buff[i] = null;
                    break;
                }
            currentSize--;
            return true;
        }
        return false;
    }

    public  boolean remove(int index) {
        for (int i = 0; i < size; i++) {
            for(;i < size; ++i)
                if (i == index) {
                    buff[i] = null;
                    break;
                }
            currentSize--;
            return true;
        }
        return false;
    }

    public  boolean remove() {
        int temp = currentSize;
        for (int i = 0; i < temp; i++) {
                buff[i] = null;
                currentSize--;
            head = head % buff.length;
        }
        return false;
    }

    public  void clear() {
        Arrays.fill(buff, null);
        head = tail = currentSize = 0;
    }

    public  boolean contains(T value) {
        for(int i = 0; i < buff.length; i++)
            if(buff[i] == value)
                return true;
        return false;
    }

    public  int indexOf(T value) {
        for(int i = 0; i < buff.length; i++)
            if(buff[i] == value)
                return i;
        return -1;
    }

    public  int getHead() {
        return head;
    }

    public  int getTail() {
        return tail;
    }

    // Return the capacity of buffer
    public  int capacity() {
        return buff.length;
    }

    // Return the current size of buffer
    public  int size() {
        return currentSize;
    }

    public  boolean isEmpty() {
        return this.currentSize == 0;
    }

    public  boolean isFull() {
        return this.currentSize == this.buff.length;
    }

    public synchronized  void showAll() {
        System.out.println("###########BUFFER ITEMS###########");
        for (T aBuff : buff) System.out.println("[ " + aBuff + " ]");
        System.out.println("##################################");
    }

    public String toString() {
        return "CircularBuffer(size=" + buff.length + ", head=" + head + ", tail=" + tail + ")";
    }
}