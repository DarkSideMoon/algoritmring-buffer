import java.util.Objects;

/**
 * Created by ����� on 23.10.2015.
 */
public class MyThread<T> implements Runnable{

    private static final Object monitor = new Object();

    public SimpleRingBuffer<String> buffer;

    public String item; // T
    public Thread thrd;
    public String threadName;
    public static int countThread;
    public boolean isWait;
    public int count = 0;

    public  MyThread(String name, String item, int count) { // T
        buffer = new SimpleRingBuffer<>(count);

        this.item = item;
        this.threadName = name;
        isWait = false;

        countThread++;
    }

    private boolean isWaitBuffer() throws InterruptedException {

        while(!this.buffer.isEmpty()) {
            isWait = true;
            wait();
        }

        return false;
    }

    public void addToBuffer() throws InterruptedException {
        count++;
        isWait = false;
        synchronized (monitor) {
            buffer.add(item + " " + count);
        }
    }

    @Override
    public void run() {
        try {
            for(int i = 0; i < 5; i++) {
                // Let the thread sleep for a while.
                addToBuffer();
                Thread.sleep(100);
            }
            System.out.println("********************New item add to buffer!********************");
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadName + " interrupted.");
        }
    }

    public void start() {
        System.out.println("Starting the thread: " +  threadName);
        if (thrd == null)
        {
            thrd = new Thread (this, threadName);
            thrd.start();
        }
    }

    public int countBuffer() {
        return buffer.size();
    }

    public int getCountThread(){
        return this.countThread;
    }

    public void stop(){
        thrd.interrupt();
    }

}
