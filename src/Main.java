/**
 * Created by ����� on 23.10.2015.
 */
public class Main {
    public static void main(String[] args){
        String no = "---------------------";
        MyThread<String> thread = new MyThread<>("1", "The thread 1 generate message", 6);
        thread.start();

        MyThread<String> thread2 = new MyThread<>("2", "The thread 2 generate message", 15);
        thread2.start();


        try {
            thread.thrd.join();
            System.out.println("The thread " + thread.threadName + " finished work");

            thread2.thrd.join();
            System.out.println("The thread " + thread2.threadName + " finished work");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.buffer.showAll();
        thread2.buffer.showAll();

        System.out.println("----------------------------------------------------------------------------------------");

        for(int i = 0; i < thread.buffer.getHead(); i++) {
            // Del elements from 1 buffer
            String res = thread.buffer.get();
            thread.buffer.remove(res);
            //System.out.println("Message deleted: " + res);

            thread2.buffer.add(res + " message from 1 thread");

        }
        Integer capacity = thread.buffer.size();

        thread.buffer.showAll();
        thread2.buffer.showAll();
        //thread.stop();
        //thread2.stop();


    }
}
