/**
 * Created by ����� on 11.10.2015.
 */
public interface IRingBuffer<T> {

    boolean isEmpty();
    boolean contains(T value);

    void add(T value);

    T get();

    int indexOf(T value);

    boolean remove(T value);
    void clear();

    int getHead();
    int getTail();
    int capacity();

    void showAll();

}
