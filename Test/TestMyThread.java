import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ����� on 23.10.2015.
 */
public class TestMyThread {

    @Test
    public void TestCreateThread() {
        MyThread<String> thread = new MyThread<>("1", "The thread 1 generate message", 100);
        thread.start();

        MyThread<String> thread2 = new MyThread<>("2", "The thread 2 generate message", 100);
        thread2.start();
    }

    @Test
    public void TestCountItems() throws InterruptedException {
        MyThread<String> thread = new MyThread<>("1", "My message", 5);
        thread.start();

        thread.thrd.join();

        Integer res = thread.buffer.size();

        Assert.assertEquals(5, res.intValue());
    }
}
