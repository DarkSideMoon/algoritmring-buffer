import org.junit.Assert;
import org.junit.Test;

import java.nio.BufferOverflowException;

/**
 * Created by ����� on 11.10.2015.
 */
public class TestSimpleRingBufferString {

    @Test
    public void testAdd() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(5);

        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");

        Integer head = buffer.getHead();

        Assert.assertEquals(head.intValue(), 3);
    }

    @Test
    public void testAddOverFlow() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(3);

        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");
        //buffer.add("Four");

        Integer head = buffer.getHead();
        Assert.assertEquals(head.intValue(), 0);
    }

    @Test
    public void testGet() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(5);
        // FIFO Qeuen
        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");
        buffer.add("Four");

        String res = buffer.get();
        buffer.get();
        buffer.get();
        buffer.get();

        Integer size = buffer.size();

        Assert.assertEquals(res, "One");
    }

    @Test
    public void testClear() {
        SimpleRingBuffer<Integer> buffer = new SimpleRingBuffer<>(5);

        buffer.add(1);
        buffer.add(2);
        buffer.add(3);

        buffer.clear();

        Integer res = buffer.size();

        Assert.assertEquals(res.intValue(), 0);
    }

    @Test
    public void testIndexOf() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(5);

        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");

        Integer res = buffer.indexOf("Two");

        Assert.assertEquals(res.intValue(), 1);
    }

    @Test
    public void testRemoveByValue() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(5);

        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");

        buffer.remove("Two");

        //buffer.showAll();
        //Integer res = buffer.size();

        Assert.assertEquals(buffer.size(), 2);
    }

    @Test
    public void testRemove() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(5);

        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");

        for(int i = 0; i < buffer.size(); i++)
            buffer.remove();

        //buffer.showAll();
        Integer res = buffer.size();

        Assert.assertEquals(buffer.size(), 0);
    }

    @Test
    public void testRemoveByIndex() {
        SimpleRingBuffer<String> buffer = new SimpleRingBuffer<>(5);

        buffer.add("One");
        buffer.add("Two");
        buffer.add("Three");

        buffer.remove(2);

        buffer.showAll();
        //Integer res = buffer.size();

        Assert.assertEquals(buffer.size(), 2);
    }
}
